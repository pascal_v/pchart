/*
 * Copyright 2013 Pascal Vogt and others.
 * 
 * This file is part of pchart.
 *
 * pchart is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pchart is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with pchart.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 * - Edorex Informatik AG
 */
package ch.entities.charting;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.imageio.ImageIO;

import com.google.gson.JsonObject;

public class TestChart3 {

	public static void main(String[] args) throws NotEnoughSpaceException {
		// Sample data
		String[] axisNames = new String[] {
			"First axis name test 3",
			"Second axis name test 3"
		};
		Double[][][] series = new Double[][][] {
			new Double[][] {
				new Double[31]
			}
		};
		Color[][] colors = new Color[][] {
			new Color[] {
				Color.ORANGE
			}
		};
		boolean[][] dashed = null;
		Date[] dates = new Date[31];
		Calendar cal = new GregorianCalendar(2012, Calendar.SEPTEMBER, 10);
		
		String[] eventNames = new String[] {};
		String[] eventTypeNames = new String[] {};
		int[] eventTypes = new int[] {};
		Color[] eventTypeColors = new Color[] {};
		boolean[][] eventSeries = new boolean[][] {};
		for (int i = 0; i < dates.length; i++) {
			dates[i] = cal.getTime();
			cal.add(Calendar.DATE, 1);
			series[0][0][i] = i % 2 == 0 ? 0.01e-30d : 0.0d;
		}
		
		String[] axisSuffix = new String[] {
			""
		};
		
		int width = 650;
		int height = 450;
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = image.createGraphics();
		
		ChartConfig chartConfig = new ChartConfig();
		
		AbstractChart c = new LineChart(chartConfig, dates, series, colors, dashed, true, axisSuffix, eventNames, eventTypeNames, eventTypes, eventTypeColors, eventSeries, axisNames);
		JsonObject renderInfos = c.render(g, width, height);
		System.out.println(renderInfos);
		
		try {
			ImageIO.write(image, "png", new File("graph"+TestChart3.class.getSimpleName()+".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
