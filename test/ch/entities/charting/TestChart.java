/*
 * Copyright 2013 Pascal Vogt and others.
 * 
 * This file is part of pchart.
 *
 * pchart is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pchart is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with pchart.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 * - Edorex Informatik AG
 */
package ch.entities.charting;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.imageio.ImageIO;

import com.google.gson.JsonObject;

public class TestChart {

	public static void main(String[] args) throws NotEnoughSpaceException {
		// Sample data
		String[] axisNames = new String[] {
			"First axis name test 1",
			"Second axis name test 1"
		};
		Double[][][] series = new Double[][][] {
			new Double[][] {
				new Double[] { 0.1d, 0.2d, 0.3d, 0.4d, 0.5, 0.6d, 0.8d, 0.9d },
				new Double[] { -0.1d, -0.2d, -0.4d, -0.8d, -1.6d, -3.2d, -3.2d, -3.2d }
			},
			new Double[][] {
				new Double[] { 200d, 300d, 900d, 2000d, null, -1000d, -1000d, -1200d },
				new Double[] { -200d, -300d, null, null, null, 900d, 920d, 940d }
			}
		};
		Color[][] colors = new Color[][] {
			new Color[] {
				Color.RED,
				Color.BLUE
			},
			new Color[] {
				Color.GREEN,
				Color.ORANGE
			}
		};
		Date[] dates = new Date[] {
			new GregorianCalendar(2013, Calendar.JUNE, 1).getTime(),
			new GregorianCalendar(2013, Calendar.JUNE, 2).getTime(),
			new GregorianCalendar(2013, Calendar.JUNE, 3).getTime(),
			// Note that 04.06.2013 is missing here -> time jump test
			new GregorianCalendar(2013, Calendar.JUNE, 5).getTime(),
			new GregorianCalendar(2013, Calendar.JUNE, 6).getTime(),
			// Note that 04.06.2013 is missing here -> time jump test
			new GregorianCalendar(2013, Calendar.JUNE, 8).getTime(),
			new GregorianCalendar(2013, Calendar.JUNE, 9).getTime(),
			new GregorianCalendar(2013, Calendar.JUNE, 10).getTime()
		};
		
		String[] axisSuffix = new String[] {
			" [min]",
			" [CHF]"
		};
		
		String[] eventNames = new String[] {
			"Ereignis A",
			"Ereignis B",
			"Ereignis B2"
		};
		String[] eventTypeNames = new String[] {
			"Typ A",
			"Typ B"
		};
		int[] eventTypes = new int[] {
			0,
			1,
			1
		};
		Color[] eventTypeColors = new Color[] {
			Color.CYAN,
			Color.MAGENTA
		};
		boolean[][] eventSeries = new boolean[][] {
			new boolean[] { false, false, true, true, false, false, false, false },
			new boolean[] { false, false, false, true, false, false, true, true },
			new boolean[] { true, true, true, true, true, true, false, true }
		};
		
		int width = 650;
		int height = 450;
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = image.createGraphics();
		
		ChartConfig chartConfig = new ChartConfig();
		//chartConfig.setHasEvents(false);
		//chartConfig.setHasTimeJumps(false);
		
		AbstractChart c = new BarChart(chartConfig, dates, series, colors, true, axisSuffix, eventNames, eventTypeNames, eventTypes, eventTypeColors, eventSeries, axisNames);
		c.setInvertedOrder(true);
		JsonObject renderInfos = c.render(g, width, height);
		System.out.println(renderInfos);
		
		try {
			ImageIO.write(image, "png", new File("graph"+TestChart.class.getSimpleName()+".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
