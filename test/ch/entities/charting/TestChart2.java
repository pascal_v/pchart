/*
 * Copyright 2013 Pascal Vogt and others.
 * 
 * This file is part of pchart.
 *
 * pchart is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pchart is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with pchart.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 * - Edorex Informatik AG
 */
package ch.entities.charting;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.imageio.ImageIO;

import com.google.gson.JsonObject;

public class TestChart2 {

	public static void main(String[] args) throws NotEnoughSpaceException {
		// Sample data
		String[] axisNames = new String[]{
			"First axis name test 2"
		};
		Double[][][] series = new Double[][][] {
			new Double[][] {
				new Double[31],
				new Double[31]
			},
			new Double[][] {
				new Double[31],
				new Double[31]
			}
		};
		Color[][] colors = new Color[][] {
			new Color[] {
				Color.ORANGE,
				Color.MAGENTA
			},
			new Color[] {
				Color.GREEN,
				Color.BLUE
			}
		};
		boolean[][] dashed = new boolean[][] {
			new boolean[] {
				false,
				true
			},
			new boolean[] {
				false,
				false
			}
		};
		Date[] dates = new Date[31];
		Calendar cal = new GregorianCalendar(2012, 8, 10);
		
		String[] eventNames = new String[] {
			"Ereignis A",
			"Ereignis B",
			"Ereignis C"
		};
		String[] eventTypeNames = new String[] {
			"Typ A",
			"Typ B",
			"Typ C"
		};
		int[] eventTypes = new int[] {
			0,
			1,
			2
		};
		Color[] eventTypeColors = new Color[] {
			Color.MAGENTA,
			Color.ORANGE,
			Color.GRAY
		};
		boolean[][] eventSeries = new boolean[][] {
			new boolean[31],
			new boolean[31],
			new boolean[31]
		};
		for (int i = 0; i < dates.length; i++) {
			dates[i] = cal.getTime();
			cal.add(Calendar.DATE, 1);
			series[0][0][i] = 5.0d * Math.sin(i * Math.PI / 2.1) + 25.0d;
			series[0][1][i] = 17.0d * (Math.sin(i * Math.PI / 15) + 1);
			series[1][0][i] = (i / 6) % 2 == 0 ? -140.0d : -300.0d;
			series[1][1][i] = -216.0d + (380.0d / 5.0d) * (((2 * i + 9) / 5) % 2 == 0 ? (2 * i + 9) % 10 : 10 - (2 * i + 9) % 10);
			eventSeries[0][i] = i <= 3;
			eventSeries[1][i] = i >= 13;
			eventSeries[2][i] = i == 23;
		}
		
		String[] axisSuffix = new String[] {
			"",
			""
		};
		
		int width = 650;
		int height = 450;
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = image.createGraphics();
		
		ChartConfig chartConfig = new ChartConfig();
		//chartConfig.setHasEvents(false);
		//chartConfig.setHasTimeJumps(false);
		
		AbstractChart c = new LineChart(chartConfig, dates, series, colors, dashed, true, axisSuffix, eventNames, eventTypeNames, eventTypes, eventTypeColors, eventSeries, axisNames);
		JsonObject renderInfos = c.render(g, width, height);
		System.out.println(renderInfos);
		
		try {
			ImageIO.write(image, "png", new File("graph"+TestChart2.class.getSimpleName()+".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
