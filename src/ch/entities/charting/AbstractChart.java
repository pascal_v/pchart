/*
 * Copyright 2013 Pascal Vogt and others.
 * 
 * This file is part of pchart.
 *
 * pchart is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pchart is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with pchart.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 * - Edorex Informatik AG
 */
package ch.entities.charting;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * This is the base class of all types of charts.
 * 
 * @author Pascal Vogt
 * @see BarChart
 * @see LineChart
 */
public abstract class AbstractChart implements Serializable {
	private static final long serialVersionUID = 1L;
	
	// constants
	protected ChartConfig config;
	
	// data
	protected Date[] dates;
	protected Double[][][] series;
	protected boolean sameZero;
	protected String[] axisSuffix;
	protected Color[][] colors;
	protected boolean[][] dashed;
	
	// events
	protected String[] eventNames;
	protected String[] eventTypeNames;
	protected int[] eventTypes;
	protected Color[] eventTypeColors;
	protected boolean[][] eventSeries;
	
	// additional state
	protected boolean invertedOrder;

	private String[] axisLabels;
	
	private static class Tuple<T,U> {
		public T first;
		public U second;
	}
	
	/**
	 * The constructor.
	 * @param config constants used when drawing the graphs
	 * @param dates the {@link Date}s to display indexed by time index
	 * @param series the values to display indexed by axis index, series index and time index.
	 * @param colors the colors of the series indexed by axis index and series index
	 * @param sameZero whether zeroes from different axis need to be aligned.
	 * @param axisSuffix and optional suffix displayed next to the axis values (like {@code "[m]"} or {@code "[kg]"})
	 * @param eventNames the names of the events indexed by event index.
	 * @param eventTypeNames the event type names indexed by event type index
	 * @param eventTypes the event type indexes corresponding to every event, indexed by event index 
	 * @param eventTypeColors the colors of every event type, indexed by event type index
	 * @param eventSeries whether or not an event is active for a given date, indexed by event index and time index.
	 * @param axisLabels The names of the value axis (y-axis) by axis index.
	 */
	public AbstractChart(ChartConfig config, Date[] dates, Double[][][] series, Color[][] colors, boolean[][] dashed, boolean sameZero, String[] axisSuffix, String[] eventNames, String[] eventTypeNames, int[] eventTypes, Color[] eventTypeColors, boolean[][] eventSeries, String[] axisLabels) {
		this.config = config;
		this.dates = dates;
		this.series = series;
		this.colors = colors;
		this.dashed = dashed;
		this.axisSuffix = axisSuffix;
		this.sameZero = sameZero;
		this.eventNames = eventNames;
		this.eventTypeNames = eventTypeNames;
		this.eventTypes = eventTypes;
		this.eventTypeColors = eventTypeColors;
		this.eventSeries = eventSeries;
		this.axisLabels = axisLabels;
	}
	
	/**
	 * Whether or not the ordering of the dates has been inverted
	 * @return
	 */
	public boolean isInvertedOrder() {
		return invertedOrder;
	}
	
	/**
	 * Draw a horizontal helper line
	 * @param g
	 * @param x
	 * @param availableSpace
	 */
	protected void drawHelperLine(Graphics2D g, int x, Rectangle availableSpace) { 
		if (x >= availableSpace.x && x <= availableSpace.x + availableSpace.width) {
			Color c = g.getColor();
			g.setColor(new Color(240, 240, 240));
			g.drawLine(x, availableSpace.y, x, availableSpace.y + availableSpace.height);
			g.setColor(c);
		}
	}
	
	protected abstract AbstractChart newInstance(Date[] dates, Double[][][] series, Color[][] colors, boolean[][] dashed, boolean sameZero, String[] axisSuffix, String[] eventNames, String[] eventTypeNames, int[] eventTypes, Color[] eventTypeColors, boolean[][] eventSeries, String[] axisNames);
	
	public AbstractChart filterSeries(Boolean[][] seriesFilter) {
		int[] nbSeriesCount = new int[seriesFilter.length];
		for (int axisIndex = 0; axisIndex < nbSeriesCount.length; axisIndex++) {
			for (Boolean visibility : seriesFilter[axisIndex]) {
				if (visibility) {
					nbSeriesCount[axisIndex]++;
				}
			}
		}
		
		Double[][][] newSeries = new Double[seriesFilter.length][][];
		Color[][] newColors = new Color[seriesFilter.length][];
		boolean[][] newDashed = new boolean[seriesFilter.length][];
		
		for (int axisIndex = 0; axisIndex < seriesFilter.length; axisIndex++) {
			int newSeriesIndex = 0;
			newSeries[axisIndex] = new Double[nbSeriesCount[axisIndex]][];
			newColors[axisIndex] = new Color[nbSeriesCount[axisIndex]];
			newDashed[axisIndex] = new boolean[nbSeriesCount[axisIndex]];
			for (int seriesIndex = 0; seriesIndex < this.series[axisIndex].length; seriesIndex++) {
				if (seriesFilter[axisIndex][seriesIndex]) {
					newColors[axisIndex][newSeriesIndex] = this.colors[axisIndex][seriesIndex];
					newSeries[axisIndex][newSeriesIndex] = this.series[axisIndex][seriesIndex];
					newDashed[axisIndex][newSeriesIndex] = this.dashed != null && this.dashed[axisIndex][seriesIndex];
					newSeriesIndex++;
				}
			}
		}
		
		return newInstance(dates, newSeries, newColors, newDashed, sameZero, axisSuffix, eventNames, eventTypeNames, eventTypes, eventTypeColors, eventSeries, axisLabels);
	}
	
	/**
	 * Is is possible to invert the ordering of the dates in the chart by passing true to this function.
	 * Calling the function again with false as a parameter will restore the original ordering.
	 * @param invertedOrder
	 */
	public void setInvertedOrder(boolean invertedOrder) {
		if (this.invertedOrder != invertedOrder) {
			this.invertedOrder = invertedOrder;
			invertOrder();
		}
	}
	
	private void invertOrder() {
		for (int i = 0; i < dates.length / 2; i++) {
			Date tmp0 = dates[i];
			dates[i] = dates[dates.length - 1 - i];
			dates[dates.length - 1 - i] = tmp0;
			for (int j = 0; j < series.length; j++) {
				for (int k = 0; k < series[j].length; k++) {
					Double tmp1 = series[j][k][i];
					series[j][k][i] = series[j][k][dates.length - 1 - i];
					series[j][k][dates.length - 1 - i] = tmp1;
				}
			}
			for (int j = 0; j < eventSeries.length; j++) {
				boolean tmp2 = eventSeries[j][i];
				 eventSeries[j][i] =  eventSeries[j][dates.length - 1 - i];
				eventSeries[j][dates.length - 1 - i] = tmp2;
			}
		}
	}
	
	/**
	 * Gets the minimum and maximum values for every axis.
	 * @return The minimum and maximum values for every axis. The first index is the axis index and the second index is 0 for the minimum and 1 for the maximum.
	 */
	private Double[][] getMinMaxs() {
		Double[][] result = new Double[series.length][];
		
		for (int axisIndex = 0; axisIndex < series.length; axisIndex++) {
			result[axisIndex] = getMinMax(series[axisIndex]);
		}
		
		return result;
	}

	/**
	 * Gets an array containing the minimum and the maximum value of the series passes as a parameter
	 * @param series the values. the first index is the series index and the second one the time index.
	 * @return {@code new Double[] { min, max }}
	 */
	private static Double[] getMinMax(Double[][] series) {
		Double min = null;
		Double max = null;
		for (Double[] s : series) {
			for (Double d : s) {
				if (d == null) {
					continue;
				}
				if (min == null || min > d) {
					min = d;
				}
				if (max == null || max < d) {
					max = d;
				}
			}
		}
		if (min == null) { // && max == null
			min = 0.0d;
			max = 1.0d;
		} else if (min > 0) {
			min = 0.0d;
		} else if (max < 0) {
			max = 0.0d;
		}
		if (min == 0.0d && max == 0.0d) {
			max = 1.0d;
		}
		return new Double[] { min, max };
	}

	/**
	 * Draws the series.
	 * @param g the {@link Graphics2D} object to use for drawing
	 * @param available the available space
	 * @param minMaxs the minimum and maximum values for every axis. the first index is the axis index and the second index is 0 for the minimum and 1 for the maximum.
	 * @param nbTimeJumps the number of "time-jumps"
	 * @return
	 */
	protected abstract JsonObject[] drawSeries(Graphics2D g, Rectangle available, Double[][] minMaxs, int nbTimeJumps);
	
	private int getNumberOfTimeHoles() {
		if (!this.config.isHasTimeJumps()) {
			return 0;
		}
 		int numberOftimeHoles = 0;
		Date oldDate = null;
		for (Date d : dates) {
			if (oldDate != null && !isConsecutiveDate(d, oldDate)) {
				numberOftimeHoles++;
			}
			oldDate = d;
		}
		
		return numberOftimeHoles;
	}
	
	/**
	 * Draws the events under the time axis
	 * @param g the {@link Graphics2D} object
	 * @param available the available space
	 * @param nbTimeJumps the number of "time-jumps"
	 */
	private JsonArray drawEvents(Graphics2D g, Rectangle available, int nbTimeJumps) {
		JsonArray drawnDays = new JsonArray();
		int h = getEventsHeight();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

		int remainingWidth = available.width - nbTimeJumps * config.getTimeJumpWidth();
		
		int timeJumpIndex = 0;
		Date oldDate = null;
		for (int dateIdx = 0; dateIdx < dates.length; dateIdx++) {
			Date d = dates[dateIdx];
			int offset = timeJumpIndex * config.getTimeJumpWidth() + available.x;
			if (oldDate != null && this.config.isHasTimeJumps() && !isConsecutiveDate(d, oldDate)) {
				timeJumpIndex++;
				offset += config.getTimeJumpWidth();
			}
			int startX = remainingWidth * dateIdx / dates.length + offset;
			int endX = remainingWidth * (dateIdx + 1) / dates.length + offset;
			int bottomY = available.y + available.height - 1;
			int topY = bottomY - h + 1;
			
			JsonObject day = new JsonObject();
			JsonArray xrange = new JsonArray();
			JsonArray yrange = new JsonArray();
			JsonArray events = new JsonArray();
			day.add("xrange", xrange);
			xrange.add(new JsonPrimitive(startX));
			xrange.add(new JsonPrimitive(endX));
			day.add("yrange", yrange);
			yrange.add(new JsonPrimitive(available.y));
			yrange.add(new JsonPrimitive(available.y + available.width));
			day.add("events", events);
			day.add("label", new JsonPrimitive(sdf.format(d)));
			drawnDays.add(day);
			
			if (config.isHasEvents()) {
				for (int eventIdx = 0; eventIdx < eventSeries.length; eventIdx++) {
					if (eventSeries[eventIdx][dateIdx]) {
						events.add(new JsonPrimitive(eventIdx));
						Color c = g.getColor();
						int eventTypeIdx = eventTypes[eventIdx];
						g.setColor(eventTypeColors[eventTypeIdx]);
						g.fillRect(startX, topY + eventTypeIdx * config.getEventThickness() + 1, endX - startX, config.getEventThickness());
						g.setColor(c);
					}
				}
				
				g.drawLine(startX, bottomY, endX, bottomY);
				g.drawLine(startX, topY, endX, topY);
			}
			oldDate = d;
		}
		
		if (config.isHasEvents()) {
			available.height -= getEventsHeight();
		}
		
		return drawnDays;
	}
	
	/**
	 * Gets the height required to render the event bar
	 * @return
	 */
	private int getEventsHeight() {
		return eventTypeNames.length * config.getEventThickness() + 2;
	}

	/**
	 * Draws the time axis
	 * @param g the {@link Graphics2D} to be used for drawing
	 * @param available the available space
	 * @param nbTimeJumps the number of "time-jumps" 
	 */
	private void drawTimeAxis(Graphics2D g, Rectangle available, int nbTimeJumps) {
		FontMetrics m = g.getFontMetrics();
		int w = m.getHeight();
		int h = m.stringWidth("99.99.9999");
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		boolean timeIsStepFunctionOfX = isTimeStepFunctionOfX();
		
		int remainingWidth = available.width - nbTimeJumps * config.getTimeJumpWidth();
		
		int timeJumpIndex = 0;
		int dontRenderDateIfXBefore = 0;
		Date oldDate = null;
		for (int i = 0; i < dates.length; i++) {
			Date d = dates[i];
			int offset = timeJumpIndex * config.getTimeJumpWidth() + available.x;
			if (oldDate != null && this.config.isHasTimeJumps() && !isConsecutiveDate(d, oldDate)) {
				timeJumpIndex++;
				offset += config.getTimeJumpWidth();
			}
			int startX = remainingWidth * i / dates.length + offset;
			int endX = remainingWidth * (i + 1) / dates.length + offset;
			int tickStartY = available.y + available.height - h - 1 - 2 * config.getDateLabelMargin();
			int tickEndY = available.y + available.height - h - config.getTickSize() - 2 * config.getDateLabelMargin();
			if (timeIsStepFunctionOfX || i == 0) {
				g.drawLine(startX, tickStartY, startX, tickEndY);
			}
			if (timeIsStepFunctionOfX || i == dates.length - 1) {
				g.drawLine(endX, tickStartY, endX, tickEndY);
			}
			if (!timeIsStepFunctionOfX) {
				g.drawLine((startX + endX) / 2, tickStartY, (startX + endX) / 2, tickEndY);
			}
			g.drawLine(startX, tickStartY, endX, tickStartY);
			int x = (startX + endX) / 2 - w / 2;
			if (x > dontRenderDateIfXBefore) {
				dontRenderDateIfXBefore = (startX + endX) / 2 + w / 2;
				if (dontRenderDateIfXBefore <= available.width + available.x) {
					AffineTransform currentTransform = g.getTransform();
					g.translate(x, available.height - config.getDateLabelMargin());
					g.rotate(-Math.PI/2);
					g.drawString(sdf.format(d), 0, m.getAscent());
					g.setTransform(currentTransform);
				}
			}
			oldDate = d;
		}
		
		available.height -= getTimeAxisHeight(g);
	}
	
	/**
	 * Whether or not the time is a step function of x (like in bar charts for example) 
	 * @return
	 */
	protected abstract boolean isTimeStepFunctionOfX();
	
	/**
	 * Draws a "time-jump". This symbolizes an interruption in the time axis.
	 * @param g the {@link Graphics2D} object
	 * @param available the available space where the "time-jump" can be drawn
	 */
	protected void drawTimeJump(Graphics2D g, Rectangle available) {
		int lastX = available.x + available.width / 2;
		int lastY = available.y + available.height;
		boolean goLeft = false;
		int loopCounter = 0;
		final int maxLoopCount = available.height;
		do {
			int nextX;
			int nextY;
			if (goLeft) {
				nextX = available.x;
			} else {
				nextX = available.x + available.width - config.getTimeJumpRiftSize();
			}
			nextY = lastY - (goLeft ?  lastX - nextX : nextX - lastX);
			if (nextY < available.y) {
				nextY = available.y;
				if (goLeft) {
					nextX = lastX - (lastY - nextY);
				} else {
					nextX = lastX + (lastY - nextY);
				}
			}
			g.drawLine(lastX, lastY, nextX, nextY);
			g.drawLine(lastX + config.getTimeJumpRiftSize(), lastY, nextX + config.getTimeJumpRiftSize(), nextY);
			lastX = nextX;
			lastY = nextY;
			goLeft = !goLeft;
			loopCounter++;
			if (loopCounter > maxLoopCount) {
				throw new IllegalStateException("suspecting infinit loop (more steps than number of pixels) -> aborting computation");
			}
		} while (lastY > available.y);
	}
	
	
	private void drawValueAxisLabel(Graphics2D g, Rectangle available, boolean alignCorrespondingAxisRight, String label) {
		FontMetrics m = g.getFontMetrics();
		int labelWidth = (label != null && !label.isEmpty()) ? m.getHeight() : 0;
		int labelHeight = labelWidth > 0 ? m.stringWidth(label) : 0;

		if (labelWidth == 0 || labelHeight == 0) {
			return;
		}
		int labelX = labelWidth;
		if (alignCorrespondingAxisRight) {
			labelX = available.x + available.width - labelWidth / 2;
		}
		
		AffineTransform originTransformation = g.getTransform();
		
		g.translate(labelX, available.height / 2 + labelHeight / 2);
		g.rotate(-Math.PI / 2);
		g.drawString(label, 0, 0);
		g.setTransform(originTransformation);
		
		available.width -= 1.5 * labelWidth;
		if (!alignCorrespondingAxisRight) {
			available.x += 1.5 * labelWidth;
		}
	}
	
	
	/**
	 * Draws a value axis
	 * @param g the {@link Graphics2D} object
	 * @param available the remaining space where the axis can be rendered
	 * @param axisDisplayValues the values to be rendered on the axis as well as the number of digits to display
	 * @param minMax the minimum and maximum values that should be able to exist according to this axis
	 * @param alignRight true if the axis should be placed on the right, false otherwise
	 * @throws NotEnoughSpaceException 
	 */
	private void drawValueAxis(Graphics2D g, Rectangle available, Tuple<List<Double>, Integer> axisDisplayValues, Double[] minMax, boolean alignRight, String axisLabel) throws NotEnoughSpaceException {
		FontMetrics m = g.getFontMetrics();
		
		
		StringBuilder formatSB = new StringBuilder("{0,number,0");
		if (axisDisplayValues.second > 0) {
			formatSB.append(".");
			for (int i = 0; i < axisDisplayValues.second; i++) {
				formatSB.append("0");
			}
		}
		formatSB.append("}");
		formatSB.append(axisSuffix[alignRight ? 1 : 0]);
		String format = formatSB.toString();
		int maxW = 0;
		for (Double d : axisDisplayValues.first) {
			int w = m.stringWidth(MessageFormat.format(format, d));
			if (w > maxW) {
				maxW = w;
			}
		}
		
		int requiredSize = maxW + config.getTickSize();
		if (requiredSize > available.width) {
			throw new NotEnoughSpaceException("Not enough space to draw value axis [requiredSize=" + requiredSize + "] [available.width=" + available.width + "]");
		}
		
		drawValueAxisLabel(g, available, alignRight, axisLabel);
		
		int lineX = available.x + (alignRight ? available.width - requiredSize : requiredSize); 
		g.drawLine(lineX, available.y, lineX, available.y + available.height);
		
		for (Double d : axisDisplayValues.first) {
			String s = MessageFormat.format(format, d);
			double v = (d - minMax[0]) / (minMax[1] - minMax[0]);
			int w = m.stringWidth(s);
			int offset = available.x + (alignRight ? available.width - maxW : 0);
			int x = maxW - w + offset;
			int y = (int)(available.height * (1 -v));
			int fixedY = y - m.getDescent() / 2 + m.getAscent() / 2;
			if (fixedY - m.getHeight() / 2 >= available.y && fixedY + m.getHeight() / 2 <= available.height) {
				g.drawString(s, x, fixedY);
				g.drawLine(lineX, y, lineX + (alignRight ? config.getTickSize() : - config.getTickSize()), y);
			}
		}
		
		available.width -= requiredSize;
		if (!alignRight) {
			available.x += requiredSize;
		}
	}
	
	/**
	 * Gets the height of the time axis
	 * @param g the {@link Graphics2D} object
	 * @return
	 */
	private int getTimeAxisHeight(Graphics2D g) {
		FontMetrics m = g.getFontMetrics();
		int h = m.stringWidth("99.99.9999") + 2 * config.getDateLabelMargin();
		
		return config.getTickSize() + h;
	}
	
	/**
	 * Returns a list of values that can be displayed in the space allocated for the value axis.
	 * @param g
	 * @param available the available space. only the height is taken into account.
	 * @param minMax the minimum and maximum values that can be displayed
	 * @return a {@link Tuple} containing a list of values to display as well as an integer indicating how many digits should be displayed after the zero 
	 */
	private Tuple<List<Double>, Integer> getValueAxisDisplayValues(Graphics2D g, Rectangle available, Double[] minMax) {
		FontMetrics m = g.getFontMetrics();
		int h = m.getHeight();

		// Here we assume that min is always negative or zero and max always positive or zero
		boolean absMinIsLarger = -minMax[0] > minMax[1];
		double value = minMax[1] - minMax[0];
		
		// We are trying to find an upper bound for the max-min difference that is also a power of 10, optionally multiplied by either 5 or 2 
		int a0 = value > 1 ? -1 : 1;
		int b0 = value > 1 ? -1 : 1;
		int loopCounter = 0;
		final int maxLoopCount = 400;
		boolean done = false;
		do {
			// upper bound is of the form 5**a0 * 2**b0
			// lower bound is of the form 5**(a0-1) * 2**(b0-1)
			// -> upperBound = 10 * lowerBound
			double lowerBoundA = Math.pow(5, a0 - 1);
			double upperBoundA = Math.pow(5, a0);
			double lowerBoundB = Math.pow(2, b0 - 1);
			double upperBoundB = Math.pow(2, b0);
			if (lowerBoundA < 0 || upperBoundA < 0 || lowerBoundB < 0 || upperBoundB < 0 || lowerBoundA * lowerBoundB * 10 < 0 || upperBoundA * upperBoundB < 0 || value * 5 < 0) {
				throw new IllegalStateException("overflow detected");
			}
			if (value > upperBoundA * upperBoundB) {
				// We have value > upperBound
				// -> value > lowerBound * 10
				if (value <= upperBoundA * upperBoundB * 2) {
					// We have: uperBound < value <= upperBound * 2
					// -> lowerBound * 10 < value <= upperBound * 2 (using upperBound = 10 * lowerBound)
					// -> lowerBound * 2 <= value <= upperBound * 2 (using lowerBound * 2 <= lowerBound * 10)
					b0++;
					done = true;
				} else if (value <= upperBoundA * upperBoundB * 5) {
					// We have: uperBound < value <= upperBound * 5
					// -> lowerBound * 10 < value <= upperBound * 5 (using upperBound = 10 * lowerBound)
					// -> lowerBound * 5 <= value <= upperBound * 5 (using lowerBound * 5 <= lowerBound * 10)
					a0++;
					done = true;
				} else {
					a0++;
					b0++;
					// We multiplied upper and lower bound by 10
					// -> value > lowerBound (using pre-condition and oldLowerBound * 10 = newLowerBound)
					// This implies that we will either run into value > upperBound again next time or terminate
				}
			} else if (value < lowerBoundA * lowerBoundB) {
				// If get this far, we have value < lowerBound. 
				// -> value < upperBound / 10
				if (value * 5 >= lowerBoundA * lowerBoundB) {
					// We have: lowerBound / 5 <= value < lowerBound
					// -> lowerBound / 5 <= value < upperBound / 10 (using upperBound = 10 * lowerBound)
					// -> lowerBound / 5 <= value <= upperBound / 5 (using upperBound / 10 <= upperBound / 5)
					// Thus by dividing upper and lower bound by 5 we are done 
					a0--;
					done = true;
				} else if (value * 2 >= lowerBoundA * lowerBoundB) {
					// We have: lowerBound / 2 <= value < lowerBound
					// -> lowerBound / 2 <= value < upperBound / 10 (using upperBound = 10 * lowerBound)
					// -> lowerBound / 2 <= value <= upperBound / 2 (using upperBound / 10 < upperBound / 2)
					// Thus by dividing upper and lower bound by 5 we are done
					b0--;
				} else {
					a0--;
					b0--;
					// We divided upper and lower bound by 10
					// -> value < upperBound (using pre-condition and oldUpperBound / 10 = newUperBound)
					// This implies that we will either run into value < lowerBound again next time or terminate
				}
			} else {
				done = true;
			}
			loopCounter++;
			if (loopCounter > maxLoopCount) {
				throw new IllegalStateException("suspecting infinit loop -> aborting computation");
			}
		} while (!done);
		
		// increment = 5**a0 * 2**b0 / (5**a1 * 2**b1)
		int maxNbOfValuesToDisplay = (int)((Math.pow(5, a0) * Math.pow(2, b0) / value) * available.height / h);
		int a1 = 0;
		int b1 = 0;
		int allowedDifference = a0 - b0;
		int nbValues = 2;
		done = false;
		loopCounter = 0;
		do {
			if (a1 <= b1 + allowedDifference && (nbValues - 1) * 5 + 1 < maxNbOfValuesToDisplay) {
				a1++;
				nbValues = (nbValues - 1) * 5 + 1;
			} else if (b1 + allowedDifference <= a1 && (nbValues - 1) * 2 + 1 < maxNbOfValuesToDisplay) {
				b1++;
				nbValues = (nbValues - 1) * 2 + 1;
			} else {
				done = true;
			}
			if (nbValues < 0) {
				throw new IllegalStateException("integers are too small for this computation (or we have just found a bug which is much more likely)");
			}
			loopCounter++;
			if (loopCounter > maxLoopCount) {
				throw new IllegalStateException("suspecting infinit loop -> aborting computation");
			}
		} while (!done);
		double increment = Math.pow(5, a0 - a1) * Math.pow(2, b0 - b1);
		if (absMinIsLarger) {
			increment = -increment;
		}
		
		List<Double> values = new ArrayList<Double>();
		for (int i = nbValues - 1; i >= 0; i--) {
			values.add(increment * i);
		}
		double v = 0.0d;
		done = false;
		loopCounter = 0;
		do {
			v -= increment;
			if (absMinIsLarger) {
				done = v > minMax[1];
			} else {
				done = v < minMax[0];
			}
			if (!done) {
				values.add(v);
			}
			loopCounter++;
			if (loopCounter > maxLoopCount) {
				throw new IllegalStateException("suspecting infinit loop -> aborting computation");
			}
		} while (!done);
		
		int da = a1 - a0;
		int db = b1 - b0;
		int digitsAfterZero;
		if (da < 0 && db < 0) {
			digitsAfterZero = 0;
		} else {
			digitsAfterZero = Math.max(da, db);
		}
		
		Tuple<List<Double>, Integer> result = new Tuple<List<Double>, Integer>();
		result.first = values;
		result.second = digitsAfterZero;
		
		return result;
	}
	
	/**
	 * Whether two dates are consecutive or not
	 * @param d0 the first {@link Date}
	 * @param d1 the second {@link Date}
	 * @return true if they are consecutive, false otherwise
	 */
	protected boolean isConsecutiveDate(Date d0, Date d1) {
		Calendar cal0 = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		cal0.setTime(d0);
		cal1.setTime(d1);
		if (cal0.compareTo(cal1) > 0) {
			cal0.add(Calendar.DATE, -2);
			return cal0.compareTo(cal1) < 0;
		} else if (cal0.compareTo(cal1) < 0) {
			cal0.add(Calendar.DATE, 2);
			return cal0.compareTo(cal1) > 0;
		} else {
			return false;
		}
	}
	
	/**
	 * Converts from values in the [ range[0], range[1] ] to the range [0, 1]
	 * @param value the value to convert
	 * @param range the range of the value
	 * @return the converted value
	 */
	protected Double valueRangeTransform(Double value, Double[] range) {
		return value != null ? ((value - range[0]) / (range[1] - range[0])) : null;
	}

	public JsonObject render(Graphics2D g, int width, int height) throws NotEnoughSpaceException {
		JsonObject renderInfo = new JsonObject();
		
		// Output some informations about the graph
		renderInfo.addProperty("type", getGraphType());
		JsonArray eventNames = new JsonArray();
		for (String eventName : this.eventNames) {
			eventNames.add(new JsonPrimitive(eventName));
		}
		renderInfo.add("event-names", eventNames);
		JsonArray eventTypeNames = new JsonArray();
		for (String eventTypeName : this.eventTypeNames) {
			eventTypeNames.add(new JsonPrimitive(eventTypeName));
		}
		renderInfo.add("event-type-names", eventTypeNames);
		JsonArray eventTypes = new JsonArray();
		for (int type : this.eventTypes) {
			eventTypes.add(new JsonPrimitive(type));
		}
		renderInfo.add("event-to-type", eventTypes);
		
		Double[][] minMaxs = getMinMaxs();
		if (sameZero && minMaxs.length > 1) {
			MinMaxUtil.makeSameZero(minMaxs);
		}
		
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);
		g.setColor(Color.BLACK);
		
		// Set up initial available space
		Rectangle availableSpace = new Rectangle(0, 0, width, height);
		if (availableSpace.width < 0 || availableSpace.height < 0) {
			throw new NotEnoughSpaceException("not enough space to draw a chart [width=" + availableSpace.width  + "] [height=" + availableSpace.height + "]");
		}
		
		// Draw all value axis
		int timeAxisHeight = getTimeAxisHeight(g);
		int eventsHeigth = getEventsHeight();
		if (availableSpace.height < (timeAxisHeight + eventsHeigth)) {
			throw new NotEnoughSpaceException("not enough space to draw time axis and events [requiredHeight=" + (timeAxisHeight + eventsHeigth) + "] [availableSpace.height=" + availableSpace.height + "]");
		}
		availableSpace.height -= timeAxisHeight + eventsHeigth;
		for (int axisIndex = 0; axisIndex < series.length; axisIndex++) {
			boolean alignRight = axisIndex >= series.length - series.length / 2;
			String axisDescription = axisIndex<axisLabels.length?axisLabels[axisIndex]:null;
			Tuple<List<Double>, Integer> axisDisplayValues = getValueAxisDisplayValues(g, availableSpace, minMaxs[axisIndex]);
			drawValueAxis(g, availableSpace, axisDisplayValues, minMaxs[axisIndex], alignRight, axisDescription);			
		}
		availableSpace.height += timeAxisHeight + eventsHeigth;
		
		// Draw the time axis
		int numberOfTimeHoles = getNumberOfTimeHoles();
		if (availableSpace.width < numberOfTimeHoles * config.getTimeJumpWidth()) {
			throw new NotEnoughSpaceException("not enough space to draw time jumps [requiredWidth=" + numberOfTimeHoles * config.getTimeJumpWidth() + "] [availableSpace.widtht=" + availableSpace.width + "]");
		}
		JsonArray formatedDates = drawEvents(g, availableSpace, numberOfTimeHoles);
		drawTimeAxis(g, availableSpace, numberOfTimeHoles);
		renderInfo.add("dates", formatedDates);
		
		// Draw the series
		JsonObject[] drawnElements = drawSeries(g, availableSpace, minMaxs, numberOfTimeHoles);
		JsonArray objects = new JsonArray();
		for (JsonObject o : drawnElements) {
			objects.add(o);
		}
		renderInfo.add("objects", objects);
		
		return renderInfo;
	}

	/**
	 * A String uniquely identifying the type of graph that is being rendered.
	 * @return
	 */
	protected abstract String getGraphType();
}
