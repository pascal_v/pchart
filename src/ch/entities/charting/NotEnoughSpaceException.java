package ch.entities.charting;

public class NotEnoughSpaceException extends Exception {
	private static final long serialVersionUID = 1L;

	public NotEnoughSpaceException(String message) {
		super(message);
	}
	
	public NotEnoughSpaceException(String message, Throwable cause) {
		super(message, cause);
	}
}
