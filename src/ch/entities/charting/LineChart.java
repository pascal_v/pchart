/*
 * Copyright 2013 Pascal Vogt and others.
 * 
 * This file is part of pchart.
 *
 * pchart is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pchart is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with pchart.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 * - Edorex Informatik AG
 */
package ch.entities.charting;

import java.awt.*;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class LineChart extends AbstractChart {
	private static final long serialVersionUID = 1L;

	/**
	 * @see AbstractChart#AbstractChart(ChartConfig, Date[], Double[][][], Color[][], boolean[][], boolean, String[], String[], String[], int[], Color[], boolean[][], String[])
	 */
	public LineChart(ChartConfig config, Date[] dates, Double[][][] series, Color[][] colors, boolean[][] dashed, boolean sameZero, String[] axisSuffix, String[] eventNames, String[] eventTypeNames, int[] eventTypes, Color[] eventTypeColors, boolean[][] eventSeries, String[] axisNames) {
		super(config, dates, series, colors, dashed, sameZero, axisSuffix, eventNames, eventTypeNames, eventTypes, eventTypeColors, eventSeries, axisNames);
	}

	@Override
	protected JsonObject[] drawSeries(Graphics2D g, Rectangle available, Double[][] minMaxs, int nbTimeJumps) {
		int nbSeries = 0;
		for (Double[][] series_ : series) {
			nbSeries += series_.length;
		}
		JsonObject[] drawnElements = new JsonObject[nbSeries * dates.length];
		int drawnElementIndex = 0;
		int remainingWidth = available.width - nbTimeJumps * config.getTimeJumpWidth();
		Date oldDate = null;
		int timeJumpIndex = 0;

		// Draw the dots
		for (int dateIndex = 0; dateIndex < dates.length; dateIndex++) {
			int offset = config.getTimeJumpWidth() * timeJumpIndex + available.x;
			Date d = dates[dateIndex];
			boolean wasTimeJump = false;
			if (oldDate != null && config.isHasTimeJumps() && !isConsecutiveDate(d, oldDate)) {
				drawTimeJump(g, new Rectangle((remainingWidth * dateIndex) / dates.length + offset, available.y, config.getTimeJumpWidth(), available.height));
				timeJumpIndex++;
				offset += config.getTimeJumpWidth();
				wasTimeJump = true;
			}
			oldDate = d;
			int previousStartX = (remainingWidth * (dateIndex - 1)) / dates.length + offset;
			int startX = (remainingWidth * dateIndex) / dates.length + offset;
			int endX = (remainingWidth * (dateIndex + 1)) / dates.length + offset;
			int centerX = (startX + endX) / 2;
			if (!wasTimeJump && config.isHasHelperLines()) {
				drawHelperLine(g, centerX, available);
			}
			int previousCenterX = (previousStartX + startX) / 2;
			Rectangle availableSpaceForDot = new Rectangle(startX, available.y, endX - startX, available.height);
			Rectangle availableSpaceForLine = new Rectangle(previousCenterX, available.y, centerX - previousCenterX, available.height);
			for (int axisIndex = 0; axisIndex < series.length; axisIndex++) {
				for (int seriesIndex = 0; seriesIndex < series[axisIndex].length; seriesIndex++) {
					Double originalValue = series[axisIndex][seriesIndex][dateIndex];
					Double value = valueRangeTransform(originalValue, minMaxs[axisIndex]);
					if (!wasTimeJump && dateIndex > 0 && series[axisIndex][seriesIndex][dateIndex - 1] != null) {
						Double oldOriginalValue = series[axisIndex][seriesIndex][dateIndex - 1];
						Double oldValue = valueRangeTransform(oldOriginalValue, minMaxs[axisIndex]);
						drawLine(g, oldValue, value, colors[axisIndex][seriesIndex], dashed != null && dashed[axisIndex][seriesIndex], availableSpaceForLine);
					}
					drawnElements[drawnElementIndex++] = drawDot(g, value, originalValue, colors[axisIndex][seriesIndex], availableSpaceForDot, axisIndex, seriesIndex, dateIndex);
				}
			}
		}
		return drawnElements;
	}

	private void drawLine(Graphics2D g, Double oldValue, Double value, Color color, boolean dashed, Rectangle available) {
		if (oldValue != null && value != null) {
			Color oldColor = g.getColor();
			Stroke oldStroke = g.getStroke();
			g.setColor(color);
			int startY = (int)(available.y + available.height * (1.0d - oldValue));
			int endY = (int)(available.y + available.height * (1.0d - value));

			if (dashed) {
				Stroke stroke = new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1.0f, new float[]{ 4f, 4f }, 2.0f);
				g.setStroke(stroke);
			}

			g.drawLine(available.x, startY, available.x + available.width - 1, endY);

			g.setColor(oldColor);
			g.setStroke(oldStroke);
		}
	}

	private JsonObject drawDot(Graphics2D g, Double value, Double originalValue, Color color, Rectangle available, int axisIndex, int seriesIndex, int dateIndex) {
		JsonObject drawnElement = new JsonObject();
		drawnElement.add("value", originalValue != null ? new JsonPrimitive(originalValue) : JsonNull.INSTANCE);
		drawnElement.addProperty("seriesIndex", seriesIndex);
		drawnElement.addProperty("axis", axisIndex);
		JsonArray xrange = new JsonArray();
		JsonArray yrange = new JsonArray();
		drawnElement.add("xrange", xrange);
		drawnElement.add("yrange", yrange);
		drawnElement.add("timeIndex", new JsonPrimitive(dateIndex));
		if (value != null) {
			Color c = g.getColor();
			g.setColor(color);
			int size = 4;
			int startX = available.x + available.width / 2 - size / 2;
			int startY = (int)(available.y + available.height * (1 - value)) - size / 2;
			g.fillRect(startX, startY, size, size);
			xrange.add(new JsonPrimitive(startX));
			xrange.add(new JsonPrimitive(startX + size));
			yrange.add(new JsonPrimitive(startY));
			yrange.add(new JsonPrimitive(startY + size));
			g.setColor(c);
		} else {
			xrange.add(new JsonPrimitive(available.x));
			xrange.add(new JsonPrimitive(available.x + available.width - 1));
			yrange.add(new JsonPrimitive(available.y));
			yrange.add(new JsonPrimitive(available.y + available.height - 1));
		}
		return drawnElement;
	}

	@Override
	protected String getGraphType() {
		return "line-chart";
	}
	
	@Override
	protected boolean isTimeStepFunctionOfX() {
		return false;
	}

	@Override
	protected AbstractChart newInstance(Date[] dates, Double[][][] series, Color[][] colors, boolean[][] dashed, boolean sameZero, String[] axisSuffix, String[] eventNames, String[] eventTypeNames, int[] eventTypes, Color[] eventTypeColors, boolean[][] eventSeries, String[] axisNames) {
		return new LineChart(config, dates, series, colors, dashed, sameZero, axisSuffix, eventNames, eventTypeNames, eventTypes, eventTypeColors, eventSeries, axisNames);
	}
}
