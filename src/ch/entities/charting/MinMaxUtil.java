/*
 * Copyright 2013 Pascal Vogt and others.
 * 
 * This file is part of pchart.
 *
 * pchart is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pchart is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with pchart.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 * - Edorex Informatik AG
 */
package ch.entities.charting;

import java.util.Arrays;
import java.util.Comparator;

public class MinMaxUtil {
	private static class MinMaxInfo {
		// the absolute negative maximum
		public double N;
		// the absolute positive maximum
		public double P;
		// P+N
		public double T;
		// P/N
		public double r;
		// N/P
		public double r_inv;
		// a factor to say that this graph is more important
		public double a;
		// the index in the original array
		public int index;
	}
	
	private static class MinMaxInfoSorter implements Comparator<MinMaxInfo> {

		@Override
		public int compare(MinMaxInfo o1, MinMaxInfo o2) {
			if (o1.P * o2.N == o2.P * o1.N) {
				return 0;
			}
			return o1.P * o2.N > o2.P * o1.N ? 1 : -1;
		}
		
	}

	public static void makeSameZero(Double[][] minMaxs) {
		/*
		 * Reformat the data in a form usable by this algorithm
		 * and to sort it such that r_i = P_i/N_i <= P_{i+1}/N_{i+1} = r_{i+1}
		 */
		MinMaxInfo[] infos = new MinMaxInfo[minMaxs.length];
		for (int i = 0; i < minMaxs.length; i++) {
			infos[i] = new MinMaxInfo();
			infos[i].index = i;
			infos[i].P = minMaxs[i][1];
			infos[i].N = -minMaxs[i][0];
			infos[i].T = infos[i].P + infos[i].N;
			infos[i].r = infos[i].P / infos[i].N;
			infos[i].r_inv = infos[i].N / infos[i].P;
			// sometimes we get negative infinity here -> fix it
			if (Double.isInfinite(infos[i].r)) {
				infos[i].r = Double.POSITIVE_INFINITY;
			}
			if (Double.isInfinite(infos[i].r_inv)) {
				infos[i].r_inv = Double.POSITIVE_INFINITY;
			}
			infos[i].a = 1.0d; // not used yet
		}
		Arrays.sort(infos, new MinMaxInfoSorter());
		
		/*
		 * Basically we are trying to minimize the following sum:
		 *  sum_i(a_i * Delta_i / (P_i + N_i))
		 * 
		 * Every Delta_i has one constraint which is either:
		 *  r = (P_i + Delta_i) / N_i
		 * or:
		 *  r = P_i / (N_i + Delta_i)
		 *  
		 * Additionally we have the constraints that we need Delta_i >= 0 for every i,
		 * which is a pretty annoying constraint. We do however know which Delta_i needs
		 * to be added to P_i and which to N_i since we sorted the r_i earlier. We just
		 * need to make sure that the r we are looking for is in the range [r_k; r_{k+1}] 
		 */
		Double optimal_r = null;
		Double minimum = null;
		int optimal_k = -1;
		for (int k = -1; k < infos.length; k++) {
			/*
			 * optimum_r is the r of an optimum found using a derivative (it doesn't have to be a
			 * minumum (could just as well be a maximum).
			 * 
			 * Also we are not sure that optimum_r will be within the bounds [r_k; r_{k+1}] and so
			 * we must consider the values at the bounds too.
			 */
			double lower_bound_r = k > -1 ? infos[k].r : 0.0d;
			double upper_bound_r = k + 1 < infos.length ? infos[k + 1].r : Double.POSITIVE_INFINITY;
			double upper_sum = 0.0d;
			for (int i = 0; i <= k; i++) {
				upper_sum += infos[i].a * infos[i].P / infos[i].T;
			}
			double lower_sum = 0.0d;
			for (int i = k + 1; i < minMaxs.length; i++) {
				lower_sum += infos[i].a * infos[i].N / infos[i].T;
			}
			double optimum_r = Math.sqrt(upper_sum / lower_sum);
			
			/*
			 * Actually computes the values we get for the optimum and for the bounds in order to compare
			 * them.
			 */
			double value_lower_bound = 0.0d;
			double value_upper_bound = 0.0d;
			double value_optimum = 0.0d;
			for (int i = 0; i <= k; i++) {
				value_lower_bound += infos[i].a * (lower_bound_r * infos[i].N - infos[i].P) / infos[i].T;
				value_upper_bound += infos[i].a * (upper_bound_r * infos[i].N - infos[i].P) / infos[i].T;
				value_optimum += infos[i].a * (optimum_r * infos[i].N - infos[i].P) / infos[i].T;
			}
			for (int i = k + 1; i < minMaxs.length; i++) {
				value_lower_bound += infos[i].a * (infos[i].P - lower_bound_r * infos[i].N) / (lower_bound_r * infos[i].T);
				value_upper_bound += infos[i].a * (infos[i].P - upper_bound_r * infos[i].N) / (upper_bound_r * infos[i].T);
				value_optimum += infos[i].a * (infos[i].P - optimum_r * infos[i].N) / (optimum_r * infos[i].T);
			}
			/*
			 * Find which of optimum_r, r_k or r_{k+1} produces the minimal value.
			 * 
			 * But remember, we need to continue looping over the other possible values of k
			 * in order to make sure the we find the minimum for the whole problem and not only
			 * a subset of it.
			 */
			if ((lower_bound_r < optimum_r && optimum_r < upper_bound_r && value_optimum < value_lower_bound && value_optimum < value_upper_bound)&& !Double.isNaN(value_optimum)) {
				if (minimum == null || minimum > value_optimum) {
					minimum = value_optimum;
					optimal_k = k;
					optimal_r = optimum_r;
				}
			} else if (value_lower_bound < value_upper_bound) {
				if ((minimum == null || minimum > value_lower_bound) && !Double.isNaN(value_lower_bound)) {
					minimum = value_lower_bound;
					optimal_k = k;
					optimal_r = lower_bound_r;
				}
			} else {
				if ((minimum == null || minimum > value_upper_bound) && !Double.isNaN(value_upper_bound)) {
					minimum = value_upper_bound;
					optimal_k = k;
					optimal_r = upper_bound_r;
				}
			}
		}
		/*
		 * Now that we have found our minimum we can fix our min-max ranges to all have the same zero
		 */
		if (optimal_r != null && !Double.isInfinite(optimal_r) && !Double.isNaN(optimal_r)) {
			for (int i = 0; i < infos.length; i++) {
				Double[] minMax = minMaxs[infos[i].index];
				if (i <= optimal_k) {
					minMax[1] = infos[i].N * optimal_r;
				} else { 
					minMax[0] = -infos[i].P / optimal_r;
				}
			}
		} else {
			boolean hasPositive = false;
			boolean hasNegative = false;
			for (MinMaxInfo info : infos) {
				if (info.P > 0.0d) {
					hasPositive = true;
				}
				if (info.N > 0.0d) {
					hasNegative = true;
				}
			}
			for (MinMaxInfo info : infos) {
				Double[] minMax = minMaxs[info.index];
				if (hasPositive && hasNegative) {
					if (info.P > info.N) {
						minMax[0] = -minMax[1];
					} else {
						minMax[1] = -minMax[0];
					}
				}
			}
		}
	}
}
