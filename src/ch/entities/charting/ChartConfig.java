/*
 * Copyright 2013 Pascal Vogt and others.
 * 
 * This file is part of pchart.
 *
 * pchart is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pchart is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with pchart.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 * - Edorex Informatik AG
 */
package ch.entities.charting;

import java.io.Serializable;

/**
 * Contains various constants used when drawing graphs.
 * @author Pascal Vogt
 *
 */
public class ChartConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int tickSize = 4;
	private int eventThickness = 3;
	private int timeJumpWidth = 10;
	private int timeJumpRiftSize = 3;
	private int dateLabelMargin = 2;
	private boolean hasHelperLines = true;
	private boolean hasTimeJumps = true;
	private boolean hasEvents = true;
	
	public int getTickSize() {
		return tickSize;
	}
	public void setTickSize(int tickSize) {
		this.tickSize = tickSize;
	}
	public int getEventThickness() {
		return eventThickness;
	}
	public void setEventThickness(int eventThickness) {
		this.eventThickness = eventThickness;
	}
	public int getTimeJumpWidth() {
		return timeJumpWidth;
	}
	public void setTimeJumpWidth(int timeJumpWidth) {
		this.timeJumpWidth = timeJumpWidth;
	}
	public int getTimeJumpRiftSize() {
		return timeJumpRiftSize;
	}
	public void setTimeJumpRiftSize(int timeJumpRiftSize) {
		this.timeJumpRiftSize = timeJumpRiftSize;
	}
	public int getDateLabelMargin() {
		return dateLabelMargin;
	}
	public void setDateLabelMargin(int dateLabelMargin) {
		this.dateLabelMargin = dateLabelMargin;
	}
	public boolean isHasHelperLines() {
		return hasHelperLines;
	}
	public void setHasHelperLines(boolean hasHelperLines) {
		this.hasHelperLines = hasHelperLines;
	}
	public boolean isHasTimeJumps() {
		return hasTimeJumps;
	}
	public void setHasTimeJumps(boolean hasTimeJumps) {
		this.hasTimeJumps = hasTimeJumps;
	}
	public boolean isHasEvents() {
		return hasEvents;
	}
	public void setHasEvents(boolean hasEvents) {
		this.hasEvents = hasEvents;
	}
}
