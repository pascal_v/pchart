/*
 * Copyright 2013 Pascal Vogt and others.
 * 
 * This file is part of pchart.
 *
 * pchart is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pchart is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with pchart.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 * - Edorex Informatik AG
 */
package ch.entities.charting;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * This class represents bar charts
 * @author Pascal Vogt
 *
 */
public class BarChart extends AbstractChart {
	private static final long serialVersionUID = 1L;

	/**
	 * @see AbstractChart#AbstractChart(ChartConfig, Date[], Double[][][], Color[][], boolean[][], boolean, String[], String[], String[], int[], Color[], boolean[][], String[])
	 */
	public BarChart(ChartConfig config, Date[] dates, Double[][][] series, Color[][] colors, boolean sameZero, String[] axisSuffix, String[] eventNames, String[] eventTypeNames, int[] eventTypes, Color[] eventTypeColors, boolean[][] eventSeries, String[] axisLabels) {
		super(config, dates, series, colors, null, sameZero, axisSuffix, eventNames, eventTypeNames, eventTypes, eventTypeColors, eventSeries, axisLabels);
	}

	@Override
	protected JsonObject[] drawSeries(Graphics2D g, Rectangle available, Double[][] minMaxs, int nbTimeJumps) {
		int nbSeries = 0;
		for (Double[][] series_ : series) {
			nbSeries += series_.length;
		}
		JsonObject[] drawnElements = new JsonObject[nbSeries * dates.length];
		int drawnElementIndex = 0;
		int remainingWidth = available.width - nbTimeJumps * config.getTimeJumpWidth();
		Date oldDate = null;
		int timeJumpIndex = 0;
		for (int dateIdx = 0; dateIdx < dates.length; dateIdx++) {
			int offset = config.getTimeJumpWidth() * timeJumpIndex + available.x;
			Date d = dates[dateIdx];
			final boolean wasTimeJump;
			if (oldDate != null && config.isHasTimeJumps() && !isConsecutiveDate(d, oldDate)) {
				drawTimeJump(g, new Rectangle((remainingWidth * dateIdx) / dates.length + offset, available.y, config.getTimeJumpWidth(), available.height));
				timeJumpIndex++;
				offset += config.getTimeJumpWidth();
				wasTimeJump = true;
			} else {
				wasTimeJump = false;
			}
			oldDate = d;
			int startX = (remainingWidth * dateIdx) / dates.length + offset;
			int endX = (remainingWidth * (dateIdx + 1)) / dates.length + offset;
			if (!wasTimeJump && dateIdx != 0 && config.isHasHelperLines()) {
				drawHelperLine(g, startX, available);
			}
			Rectangle availableSpaceForBar = new Rectangle(startX, available.y, endX - startX, available.height);
			/**
			 * Draw values for each axis
			 */
			int seriesOffset = 0;
			for (int axisIndex = 0; axisIndex < series.length; axisIndex++) {
				Double[] values = new Double[series[axisIndex].length];
				Double[] originalValues = new Double[series[axisIndex].length];
				for (int j = 0; j < values.length; j++) {
					Double v = series[axisIndex][j][dateIdx];
					originalValues[j] = v;
					values[j] = valueRangeTransform(v, minMaxs[axisIndex]);
				}
				Double zero = valueRangeTransform(0.0d, minMaxs[axisIndex]);
				JsonObject[] os = drawBars(g, zero, values, originalValues, colors[axisIndex], seriesOffset, nbSeries, availableSpaceForBar, 0, axisIndex >= series.length / 2, dateIdx, axisIndex);
				for (JsonObject o : os) {
					drawnElements[drawnElementIndex++] = o;
				}
				seriesOffset += series[axisIndex].length;
			}
		}
		return drawnElements;
	}

	private JsonObject[] drawBars(Graphics2D g, Double zero, Double[] values, Double[] originalValues, Color[] colors, int barOffset, int numberOfBars, Rectangle available, int margin, boolean isRightAxis, int dateIndex, int axisIndex) {
		JsonObject[] barDescriptors = new JsonObject[values.length];
		int totalWidth = available.width - 2 * margin;
		for (int idx = 0; idx < values.length; idx++) {
			int startX = (totalWidth * (idx + barOffset)) / numberOfBars + available.x;
			int endX = (totalWidth * (idx + barOffset + 1)) / numberOfBars + available.x;
			if (values[idx] != null) {
				int zeroY = (int) (available.height * zero);
				int valueY = (int) (available.height * values[idx]);
				int startY = (values[idx] > zero ? zeroY : valueY) + available.y;
				int endY =  (values[idx] > zero ? valueY : zeroY) + available.y;
				Color c = g.getColor();
				g.setColor(colors[idx]);
				g.fillRect(startX, available.y + available.height - endY, endX - startX, endY - startY - 1);
				g.setColor(c);
			}
			JsonObject barDescriptor = new JsonObject();
			JsonArray xRange = new JsonArray();
			xRange.add(new JsonPrimitive(startX));
			xRange.add(new JsonPrimitive(endX));
			barDescriptor.add("xrange", xRange);
			JsonArray yRange = new JsonArray();
			yRange.add(new JsonPrimitive(available.y));
			yRange.add(new JsonPrimitive(available.y + available.height - 1));
			barDescriptor.add("yrange", yRange);
			barDescriptor.addProperty("seriesIndex", idx);
			barDescriptor.addProperty("value", originalValues[idx]);
			barDescriptor.addProperty("axis", axisIndex);
			barDescriptor.add("timeIndex", new JsonPrimitive(dateIndex));
			barDescriptors[idx] = barDescriptor;
		}
		return barDescriptors;
	}

	@Override
	protected String getGraphType() {
		return "bar-chart";
	}

	@Override
	protected boolean isTimeStepFunctionOfX() {
		return true;
	}
	
	@Override
	protected AbstractChart newInstance(Date[] dates, Double[][][] series, Color[][] colors, boolean[][] dashed, boolean sameZero, String[] axisSuffix, String[] eventNames, String[] eventTypeNames, int[] eventTypes, Color[] eventTypeColors, boolean[][] eventSeries, String[] axisNames) {
		return new BarChart(config, dates, series, colors, sameZero, axisSuffix, eventNames, eventTypeNames, eventTypes, eventTypeColors, eventSeries, axisNames);
	}
}
